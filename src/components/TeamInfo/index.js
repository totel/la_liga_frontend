import React, { Component } from 'react';
import Player from './../Player';
import { Table, Button } from 'reactstrap';
import { Link } from 'react-router-dom'
import {
    API_URL
} from './../../constants/constants';

const api_teams = `${API_URL}/teams`;

class TeamInfo extends Component {
    
    constructor({ match }) {
        super();
        this.state = {
            match,
            players: [],
            team: null,
            loading: false,
            error: null
        };
    }  


    componentDidMount() {
        const { match } = this.props;
        const team_id = match.params.id;
        this.setState({ loading: true, team: team_id });
        const api_teams_player = `${api_teams}/${team_id}/players`;
        fetch(api_teams_player).then(data => {
            return data.json();
        }).then(players => {
            this.setState({ players, loading: false })
        });
    }


    render = () => {
        const {players, loading, team} = this.state;
        if (loading) return (<p>Cargando ...</p>);
        return (
            <div>
                <div className="button-create">
                    <Button color="primary">
                        <Link to={`/equipo/${team}/nuevo-jugador`}>Nuevo Jugador</Link> 
                    </Button>
                </div>
                

                <Table>
                    <thead>
                        <tr>
                            <th>
                                Dorsal
                            </th>
                            <th>
                                Nombre
                            </th>
                            <th>
                                Apodo
                            </th>
                            <th>
                                Posición
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <Player players={players}/>
                    </tbody>
                </Table>
            </div>
        )
    };

    
}

export default TeamInfo;