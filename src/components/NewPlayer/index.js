import React, { Component } from 'react';
import { Button, Form, FormGroup, Label, Input, Modal, ModalHeader, ModalFooter, NavLink  } from 'reactstrap';


import {
    API_URL
} from './../../constants/constants';

const api_players = `${API_URL}/players`;

class NewPlayer extends Component {

    constructor(match) {
        super();
        this.state = { 
            match,
            name: '',
            nickname: '',
            position: '',
            dorsal: '',
            team: '',
            modal: false,
            error: null
        };

        this.toggle = this.toggle.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    toggle() {
        this.setState({
            modal: !this.state.modal
        });
    }

    componentDidMount() {
        const { match } = this.props;
        this.setState({team:match.params.id})
    }
    

    handleChange(e) {
        let newState = {}
        newState[e.target.name] = e.target.value
        this.setState(newState)
    }

    handleSubmit(e) {
        e.preventDefault();
        var formData = new FormData();
        const {team, nickname, dorsal, position, name} = this.state;
        
        formData.append('name', name);
        formData.append('nickname', nickname);
        formData.append('dorsal', dorsal);
        formData.append('position', position);
        formData.append('team_id', parseInt(team,0));

        fetch(api_players, {
            method: 'POST',
            body: formData  
        }).then(data => {
            return data.json();
        }).then(player => {
            this.toggle();
            this.setState({
                name: '',
                nickname: '',
                dorsal: '',
                position: '' 
            });
        }).catch (error => this.setState({ error }));;
    }

    render = () => {
        const {error, team} = this.state;

        if (error) return (<p>Error guardando datos</p>);

        return (
            <div className="form-new-player">
                <Form onSubmit={this.handleSubmit}>
                    <FormGroup>
                        <Label>
                            Nombre:
                            <Input required type="text" name="name" value={this.state.name} onChange={this.handleChange} />
                        </Label>
                    </FormGroup>
                    <FormGroup>
                    <Label>
                        Apodo:
                        <Input required type="text" name="nickname" value={this.state.nickname} onChange={this.handleChange} />
                    </Label>
                    </FormGroup>
                    <FormGroup>
                        <Label>
                            Dorsal:
                            <Input required type="number" name="dorsal" value={this.state.dorsal} onChange={this.handleChange} />
                        </Label>
                    </FormGroup>
                    <FormGroup>
                    <Label>
                        Posición:
                        <Input required type="text" name="position" value={this.state.position} onChange={this.handleChange} />
                    </Label>
                    </FormGroup>
                    <FormGroup>    
                        <Button color="primary">Guardar</Button>
                    </FormGroup>
                </Form>

                <Button color="link"><NavLink href={`/equipo/${team}`} >Volver al equipo</NavLink></Button>

                <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
                    <ModalHeader>Jugador guardado corretamente.</ModalHeader>
                    <ModalFooter>
                        <Button color="primary" onClick={this.toggle}>Ok</Button>
                    </ModalFooter>
                </Modal>
            </div>
        )
    };


}

export default NewPlayer;