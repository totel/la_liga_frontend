import React, { Component } from 'react';


class Player extends Component {

    constructor({ players }) {
        super();
        this.state = {
            players
        };
    }

    render = () => {
        const { players } = this.state;
        return (
            players.map(player => (<tr key={player.id}><td>{player.dorsal}</td><td>{player.name}</td><td>{player.nickname}</td><td>{player.position}</td></tr>))
        )
    };


}

export default Player;