import React, { Component } from 'react';
import Team from './Team';
import {
    API_URL
} from './../../constants/constants';
const api_teams = `${API_URL}/teams`;

class TeamList extends Component {

    constructor() {
        super();
        this.state = {
            teams: [],
            loading: false,
            error: null
        };
    }

    componentDidMount() {
        this.setState({loading:true})
        fetch(api_teams).then(data => {
            return data.json();
        }).then(teams => {
            this.setState({ teams, loading: false });
        }).catch(error => this.setState({ error, loading: false }));
    }
    
    render = () => {
        const { teams, loading, error } = this.state;

        if (loading) return (<p>Cargando ...</p>);

        if (error) return (<p>Error recuperando datos</p>);

        return (
            teams.map(team => (<Team team={team} key={team.id}/>))
        );
    }
}


export default TeamList;