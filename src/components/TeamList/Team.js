import React, { Component } from 'react';
import {
    Link
} from 'react-router-dom'
import { ListGroup, ListGroupItem } from 'reactstrap';


class Team extends Component {
    constructor({ team }) {
        super();
        this.state = {
            team
        };
    }  

    render = () => {
        const {team} = this.props;
        return (
            <ListGroup>
                <ListGroupItem action>
                    <Link to={`/equipo/${team.id}`}>{team.name}</Link> 
                </ListGroupItem>       
            </ListGroup>
        );
        
    };

    
}

export default Team;