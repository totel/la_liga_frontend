import React, { Component } from 'react';
import { Navbar, NavbarBrand, Nav, NavItem, NavLink } from 'reactstrap';


class Header extends Component {

    render = () => {
        return (
            <div>
                <Navbar color="light" light>
                    <NavbarBrand href="/">LaLiga</NavbarBrand>
                    <Nav>
                        <NavItem>
                            <NavLink href="/">Equipos</NavLink>
                        </NavItem>
                    </Nav>
                </Navbar>
            </div>
        )
    };


}

export default Header;