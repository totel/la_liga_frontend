import React, { Component } from 'react';
import './App.css';
import TeamList from './components/TeamList';
import TeamInfo from './components/TeamInfo';
import NewPlayer from './components/NewPlayer';
import Header from './components/Header';
import {
  BrowserRouter as Router,
  Route,
  Switch
} from 'react-router-dom'


class App extends Component {
  render() {
    return (
      <div className="App">
        <Header/>
        <Router>
          <div>
            <Switch>
              <Route path="/" exact component={TeamList} />
              <Route path="/equipo/:id/nuevo-jugador" exact component={NewPlayer} />
              <Route path="/equipo/:id" component={TeamInfo} />
            </Switch>
          </div>
        </Router>
      </div>
    );
  }
}

export default App;
